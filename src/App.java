public class App {
    public static void main(String[] args) throws Exception {
        System.out.println("Hello, World!");
        // task 50.10
int total = sumNumbersV1();
System.out.println("Total from 1 to 100 is:  " + total);
    }
    
    public static int sumNumbersV1(){
        int sum=0;
        for (int i =1;  i<=100; i++){
            sum = sum + i;
        }
        return sum;

    }
}
